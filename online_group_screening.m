%{
Copyright (c) 2016, Tianxiang Gao All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of online-group-screening nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%}
function [b,wc_t,wc_r,s] = online_group_screening(n,tau,m,k,limit)
%%%%%%%%%%%%%%%%%%%%%%%
%   Group screening decision with pilot experiment result
%   Input:
%       n: rest total candidates
%       tau: efficiency (treatments can be conducted in one round)
%       m: pilot treatments
%       k: true agents count in pilot treatments 
%       limit: (optional) the maximum group size
%   Output:
%       b: optimal group number, if group-screening is not suggested, b = n
%       wc_t: expected total treatments needed
%       wc_r: expected total roudns needed
%%%%%%%%%%%%%%%%%%%%%%%
if nargin<1
    n = 45000;
    m = 5000;
    k = 7;
    tau = 5000;
    fprintf('Example: %d candidates, %d pilot treatments with %d true agent findings, %d treatments/round\n',n,m,k,tau);
end
if nargin<5
    limit = n;
end

verbose = 1;

naive_round = ceil(n/tau);
if verbose
    fprintf('Naive method: %d rounds, %d treatments.\n',naive_round,n);    
end
alpha = 0.5;
s = max(1,(k/m + sqrt(log(1-alpha)/(-2*m)))*n);
s = round(s);
[b,wc_t,wc_r] = group_screening(n,tau,s,limit);
end

function [b,wc_t,wc_r] = group_screening(n,tau,s,limit)
%%%%%%%%%%%%%%%%%%%%%%%
%   Group screening decision
%   Input:
%       n: total candidates
%       tau: efficiency (treatments can be conducted in one round)
%       s: true agent number (or approximate maximal true agent number)
%       verbose: 0 or 1, (optional) switch for outputing information
%   Output:
%       b: optimal group number, if group-screening is not suggested, b = n
%       wc_t: worst case total treatments needed
%       wc_r: worst case total roudns needed

%%%%%%%%%%%%%%%%%%%%%%%
if nargin<1
    n = 50000;
    s = 7;
    tau = 5760;
    fprintf('Example: %d candidates, %d true agents, %d treatments/round\n',n,s,tau);
end
if nargin<4
    limit = n;
end
verbose = 1;

naive_round = ceil(n/tau);
if verbose
%     fprintf('Naive method using %d rounds and %d treatments.\n',naive_round,n);    
end
group=0;
if s/n < 0.1716
    b = max(ceil(n/limit),round(sqrt(n*s)));
    rest = s.*floor(n/b) + min(s,mod(n,b));
    wc_r = ceil(b/tau) + ceil(rest/tau);
    wc_t = b+rest;
if wc_r<=naive_round
    if verbose
        fprintf('Group-screening: %d rounds, %d treatments.\n',wc_r,wc_t);
        fprintf('with %d groups, %d candidates max per group\n',b,ceil(n/b));
        fprintf('Group-screening is suggested in next round!');
    end
    group = 1;
end
end
if group==0
    b = n;
    wc_t = n;
    wc_r = naive_round;
    if verbose
        fprintf('Group screening is not suggested, continue naive method next round!\n');
    end
end
end
